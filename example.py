#!/usr/bin/env python                                                           

##--------------------------------                                              
import sys
import glob
import re
import os
import numpy as  np
import matplotlib.pyplot as plt
from matplotlib import rc
##--------------------------------                                              


sys.path.append('../PyCactus')


import CarpetASCIII as CARPET
import CarpetH5     as H5

dataSC = H5.ReadScalaHDF5('../RAWDATA/hdf5/Sly_ppm_14vs14_r25.h5')
datah5XY = dict()
datah5XZ = dict()

PATH='/numrel/storageQ02/WorkBNS/simulations/DePietriGalileo/'
MODEL='Sly_14vs14_r25'
SEARCH3d='/output-0000/*/data/H5_3d/*.xyz.file_0.h5'

SEARCH2dXY='/output-0000/*/data/H5_2d/*.xy.h5'
fullSEARCH2dXY = PATH + MODEL + SEARCH2dXY
descXY_FILE='hdf5/''descXY' + MODEL + '.h5'
dataXY_FILE='hdf5/''dataXY' + MODEL + '.h5'

SEARCH2dXZ='/output-0000/*/data/H5_2d/*.xz.h5'
fullSEARCH2dXZ = PATH + MODEL + SEARCH2dXZ
descXZ_FILE='hdf5/''descXZ' + MODEL + '.h5'
dataXZ_FILE='hdf5/''dataXZ' + MODEL + '.h5'

it=0;rl=5;

descXY = H5.CreateDescriptorList(fullSEARCH2dXY,descXY_FILE)
datah5XY[it,rl] = H5.CreateJoinedHDF5data(descXY,it,rl,dataXY_FILE)

descXZ = H5.CreateDescriptorList(fullSEARCH2dXZ,descXZ_FILE)
datah5XZ[it,rl] = H5.CreateJoinedHDF5data(descXZ,it,rl,dataXZ_FILE)

## h2dir = '/Users/depietri/simulations/TESTnp4/output-0000/BSSN_G300_14vs14_r75/data/H5_2d/'
## h3dir = '/Users/depietri/simulations/TESTnp4/output-0000/BSSN_G300_14vs14_r75/data/H5_3d/'
## 
## SEARCH2d='*.??.h5'
## SEARCH3d ='*.???.file_0*.h5'
## SEARCH3d0='*.???.h5'
## 
## rhoXY =H5.CreateDescriptorList(h2dir+'rho.xy.h5')
## rhoXYZ=H5.CreateDescriptorList(h3dir+'rho.xyz.file_0.h5')
## 
## allXY =H5.CreateDescriptorList(h2dir+'*.xy.h5')
## allXYZ=H5.CreateDescriptorList(h3dir+'*.xyz.file_0.h5')
## 
## it0_3d = H5.CreateJoinedHDF5data(allXYZ,0,5)
## it0_2d = H5.CreateJoinedHDF5data(allXY,0,5)
## it0_2dx = H5.Extract2dSlice(it0_3d,0,dir=2)
## fig=figure()
## ax1=fig.subplot(2,1,1)
## ax2=fig.subplot(2,1,2)
## ax1.pcolor(it0_2d['X[0]'],it0_2d['X[1]'],it0_2d['rho'])
## 
