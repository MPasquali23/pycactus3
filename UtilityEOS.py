import numpy as np
#from matplotlib import pyplot as plt
import scipy
import scipy.interpolate
import scipy.optimize
import sys
import h5py as h5
import os
from pycactus3.UtilityUnits import *


#Utility function for munu zero finding for imposing beta equilibrium
def f_munu(y,rho,table):
    interpmunu = scipy.interpolate.RectBivariateSpline(np.array(table['ye']),np.array(table['logrho']),np.array(table['munu'][:,0,:]))
    return interpmunu(y,rho)


#Reads Finite Temperature EOS tables from stellarcollapse.org and 
#converts them in the out_format, imposing beta equilibrium 
#Right now the options are:
#out_format='lorene', out_format='rns' and out_format='ascii'(slow)
#Still work in progress!
def ReadOttTable(file_in,nrows=5000,rhomin=5e6,rhomax=5e15,T=0.01,ye=0.15,beta_eq=True,out_format="ascii"):
    table = h5.File(file_in)
    name = os.path.split(file_in)[1]
    name = os.path.splitext(name)[0]
    print (name)
    if out_format == 'ascii':
    #Don't use it right now, it's extreamely slow, it needs some optimization, like proper loops ordering.
        fout = open(name+'.ascii','w')
        fout.write("Number of entries: T %d rho %d Ye %d \n" %(len(table['logtemp']), len(table['logrho']), len(table['ye'])))
        fout.write("Columns: T[Mev] rho[g/cm^3], ye, P[dyne/cm^2], e[erg/g], mu_e[Mev/baryon], mu_n[Mev/baryon], mu_hat[Mev/baryon], munu[Mev/baryon], cs2 [cm^2/s^2], Xa, Xh, Xn, Xp")
        for j,t in enumerate(table['logtemp']):
            temp = 10**t
            for i,r in enumerate(table['logrho']):
                rho = 10**r
                for k,ye in enumerate(table['ye']):
                    fout.write("%1.8e %1.8e %1.8e %1.8e %1.8e %1.8e %1.8e %1.8e %1.8e %1.8e " %(temp,rho,ye,10**table['logpress'][k,j,i],10**table['logenergy'][k,j,i]-table['energy_shift'],table['mu_e'][k,j,i],table['mu_n'][k,j,i],table['mu_p'][k,j,i],table['munu'][k,j,i],table['cs2'][k,j,i]))
        four.close()
    if (out_format == 'lorene') or (out_format == 'rns') :
        lrhomin = np.log10(rhomin)
        lrhomax=np.log10(rhomax)
        ldrho=(lrhomax-lrhomin)/(nrows-1)
        #idx_T=0 #Choice of temperature not implemented yet!
        mb = 1.66e-24
        #Imposing beta equilibrium

        if out_format=='lorene':
            fout=open(name+'_lorene.d','w')
            fout.write('#\n#\n#\n#\n#\n')
            fout.write('%d <-------number of lines\n'% nrows)
            fout.write('#\n# n_B[fm^-3] rho[g/cm^3] p[dyn/cm^2] \n#\n')
            interp_e = scipy.interpolate.RectBivariateSpline(np.array(table['ye']),np.array(table['logrho']),np.array(table['logenergy'][:,0,:]))
            interp_p = scipy.interpolate.RectBivariateSpline(np.array(table['ye']),np.array(table['logrho']),np.array(table['logpress'][:,0,:]))
            interp_munu = scipy.interpolate.RectBivariateSpline(np.array(table['ye']),np.array(table['logrho']),np.array(table['munu'][:,0,:]))
            for i in range(nrows):
                lrho_tmp=(lrhomin+i*ldrho)
                #ye_tmp=scipy.optimize.newton(f_munu,0.5,args=(lrho_tmp,table),maxiter=200)
                ye_tmp=scipy.optimize.brentq(f_munu,0.01,0.65,args=(lrho_tmp,table),maxiter=200)
                e_tmp=10**lrho_tmp*((10**interp_e(ye_tmp,lrho_tmp)[0][0]-table['energy_shift'][0])/(c**2*10000)+1)
                p_tmp=10**interp_p(ye_tmp,lrho_tmp)[0][0]
                nb_tmp = 10**lrho_tmp/mb/1e39
                munu_tmp = interp_munu(ye_tmp,lrho_tmp)[0][0]
                fout.write('%d   %1.8e    %1.8e    %1.8e\n' %(i+1,nb_tmp,e_tmp,p_tmp))
                print ('rho,eps,p,ye,munu',10**lrho_tmp/CU_to_densCGS, (e_tmp/10**lrho_tmp-1), p_tmp/CU_to_densCGS/c**2/10000, ye_tmp, munu_tmp)
            fout.close()
        elif out_format == 'rns':
            
            interp_e = scipy.interpolate.RectBivariateSpline(np.array(table['ye']),np.array(table['logrho']),np.array(table['logenergy'][:,0,:]))
            interp_p = scipy.interpolate.RectBivariateSpline(np.array(table['ye']),np.array(table['logrho']),np.array(table['logpress'][:,0,:]))
            interp_munu = scipy.interpolate.RectBivariateSpline(np.array(table['ye']),np.array(table['logrho']),np.array(table['munu'][:,0,:]))
            #fout=open(name+'_rns','w')
            fout=open(name+'_e_vs_p','w')
            #fout.write('%d\n' % nrows)
            for i in range(nrows):
                lrho_tmp=(lrhomin+i*ldrho)
                try:
                    ye_tmp=scipy.optimize.brentq(f_munu,0.01,0.5,args=(lrho_tmp,table),maxiter=200)
                except:
                    opt = scipy.optimize.minimize_scalar(f_munu,bounds=(0.01,0.5),args=(lrho_tmp,table),method="Bounded")
                    #xa,xb,xc,fa,fb,fc,calls = scipy.optimize.bracket(f_munu,0.2,0.5,args=(lrho_tmp,table))
                    print ("x,f",opt.x,f_munu(opt.x,lrho_tmp,table))
                    if f_munu(opt.x,lrho_tmp,table) > 0:
                        xr = np.arange(0.01,0.5,0.002)
                        plt.plot(xr,f_munu(xr,lrho_tmp,table))
                        plt.axvline(opt.x)
                        opt = scipy.optimize.minimize_scalar(f_munu,bounds=(0.03,0.5),args=(lrho_tmp,table),method="Bounded")
                        print ("x,f",opt.x,f_munu(opt.x,lrho_tmp,table))
                    try:
                        if ye_tmp > opt.x:
                            ye_tmp=scipy.optimize.brentq(f_munu,opt.x,0.5,args=(lrho_tmp,table),maxiter=200)
                        else:
                            ye_tmp=scipy.optimize.brentq(f_munu,0.01,opt.x,args=(lrho_tmp,table),maxiter=200)
                    except:
                        ye_tmp=scipy.optimize.brentq(f_munu,opt.x,0.5,args=(lrho_tmp,table),maxiter=200)
                e_tmp=10**lrho_tmp*((10**interp_e(ye_tmp,lrho_tmp)[0][0]-table['energy_shift'][0])/(c**2*10000)+1)
                p_tmp=10**interp_p(ye_tmp,lrho_tmp)[0][0]
                nb_tmp = 10**lrho_tmp/mb
                munu_tmp = interp_munu(ye_tmp,lrho_tmp)[0][0]
                h = (e_tmp+p_tmp/(c**2*10000))/(10**lrho_tmp)
                #print lrho_tmp,e_tmp,p_tmp,h,ye_tmp
                h_tmp = c**2*10000*np.log(h)
                #fout.write('%1.8e    %1.8e    %1.8e    %1.8e\n' %(e_tmp,p_tmp,h_tmp,nb_tmp))
                fout.write('%1.8e    %1.8e\n' %(e_tmp,p_tmp))
                print ('rho,eps,p,ye,munu',10**lrho_tmp/CU_to_densCGS, (e_tmp/10**lrho_tmp-1), p_tmp/CU_to_densCGS/c**2/10000, ye_tmp, munu_tmp)
            fout.close()
                            
#Converts a Lorene cold EOS table to a EOS_Omni one.

def lorene2eos_omni(table_in,table_out,ntab=600,rhomin=1e-9):
    lorene = np.loadtxt(table_in,comments='#',skiprows=6,usecols=(1,2,3),unpack=True)
    rho=np.zeros(ntab+1)
    rhol=1.66e-24*lorene[0]*1e39
    epsl=(lorene[1]/rhol-1)
    print (epsl)
    rhol=rhol/CU_to_densCGS
    pl=lorene[2]/(c**2*10000*CU_to_densCGS)
    rhomax=rhol[-1]
    dlrho = (np.log10(rhomax)-np.log10(rhomin))/(ntab-1)
    for i in range(ntab+1):
        rho[i]=10**(np.log10(rhomin)+i*dlrho)
    eps_spline = scipy.interpolate.UnivariateSpline(rhol,epsl,s=0,k=3)
    p_spline = scipy.interpolate.UnivariateSpline(rhol,pl,s=0,k=3)
    eps = eps_spline(rho)
    p = p_spline(rho)
    gamma = np.log(p)/np.log(rho)
    cs = np.sqrt(np.diff(p)/np.diff(rho*(1+eps)))
    out = open(table_out,"w")
    out.write("EoSType = Tabulated\n")
    out.write("Nrho = "+str(ntab)+"    NYe = 1     NT = 1\n")
    out.write("RhoMin = "+str(rhomin)+"  RhoMax = "+str(rhomax)+"\n")
    out.write("HeatCapacityE = 1\n")
    out.write("GammaTh = 1.8\n")
    out.write("Kappa = 1\n")
    out.write("RhoSpacing = Log\n")
    for i in range(ntab):
        out.write("    %1.14e    %1.14e    %1.14e\n" %(eps[i],gamma[i],cs[i]))
    out.close()

#Creates a lorene cold eos table from a GENERIC piecewise polytropic eos
#Input in cgs with c=1 (as in Read et al.)
#Use instead pp2lorene_crust for seven pieces PP EOS from Read et al. 2009

def pp2lorene(filename,gammas,kappa0,rhos,ntot=2000,rhomin=5e6,rhomax=5e15):
    kappa = np.zeros(len(gammas))
    eps0 = np.zeros(len(gammas))
    eps0[0]=0
    kappa[0]=kappa0
    for i,rho in enumerate(rhos):
        kappa[i+1]=kappa[i]*rho**(gammas[i]-gammas[i+1])
        eps0[i+1]=eps0[i]+kappa[i]/(gammas[i]-1.0)*rho**(gammas[i]-1.0)- kappa[i+1]/(gammas[i+1]-1.0)*rho**(gammas[i+1]-1.0)
    lrhomin = np.log10(rhomin)
    lrhomax = np.log10(rhomax)
    ldrho = (lrhomax-lrhomin)/(ntot-1)
    table = open(filename,'w')
    table.write('# \n # \n # \n # \n # \n')
    table.write('%d    <-- Number of lines\n' % ntot)
    table.write('#\n#     n_B [fm^{-3}]  rho [g/cm^3]   p [dyn/cm^2]\n#\n')
    for i in range(ntot):
        rho_tmp = 10**(lrhomin + i*ldrho)
        nb = rho_tmp/(1.66e-24*1e39)
        k_tmp = kappa[0]
        gamma_tmp = gammas[0]
        eps0_tmp = eps0[0]
        for j,rho in enumerate(rhos):
            if rho_tmp > rho:
                k_tmp = kappa[j+1]
                gamma_tmp = gammas[j+1]
                eps0_tmp = eps0[j+1]
        e=rho_tmp*(1.0 + k_tmp/(gamma_tmp - 1.0)*rho_tmp**(gamma_tmp-1.0) + eps0_tmp)
        p=k_tmp*rho_tmp**gamma_tmp*(c**2*10000)
        table.write('    %d    %1.15e    %1.15e    %1.15e \n' % (i+1,nb,e,p))



#Creates a lorene cold eos table from a piecewise polytropic eos
#Input in cgs with c=1 (as in Read et al.)
#Uses SLy for low density
#Uses rhos from Read et. al, calculates rho4)
#The input gammas are only the three high desity ones! 

def pp2lorene_crust(filename,hd_gamma,log10p1,ntot=2000,rhomin=5e6,rhomax=5e15):
    p1 = 10**log10p1
    kappa = np.zeros(7)
    eps0 = np.zeros(7)
    eps0[0]=0
    #Setting up crust and free fermion gas
    kappa[0]=6.80110e-9
    kappa[1]=1.06186e-6
    kappa[2]=5.32697e1
    kappa[3]=3.99874e-8
    gammas=np.zeros(7)
    gammas[0]=1.58425
    gammas[1]=1.28733
    gammas[2]=0.62223
    gammas[3]=1.35692
    rhos = np.zeros(len(gammas)-1)
    rhos[0]=2.44034e7
    rhos[1]=3.78358e11
    rhos[2]=2.62780e12
    #Calculating rho between low and high density part
    gammas[4]=hd_gamma[0]
    gammas[5]=hd_gamma[1]
    gammas[6]=hd_gamma[2]
    rhos[4]=10**14.7
    rhos[5]=1e15
    kappa[5]=p1/(c**2*10000*rhos[4]**gammas[5])
    kappa[4]=p1/(c**2*10000*rhos[4]**(gammas[4]))
    rhos[3]=(kappa[3]/kappa[4])**(1/(gammas[4]-gammas[3]))
    kappa[6]=kappa[5]*rhos[5]**(gammas[5]-gammas[6])
    print ('kappa',kappa)
    print ('rho',rhos)
    print ('gamma',gammas)
    print ('pl', [kappa[i]*rhos[i]**gammas[i] for i in range(6)])
    print ('pr', [kappa[i+1]*rhos[i]**gammas[i+1] for i in range(6)])
    for i,rho in enumerate(rhos):
        eps0[i+1]=eps0[i]+kappa[i]/(gammas[i]-1.0)*rho**(gammas[i]-1.0)- kappa[i+1]/(gammas[i+1]-1.0)*rho**(gammas[i+1]-1.0)
    lrhomin = np.log10(rhomin)
    lrhomax = np.log10(rhomax)
    ldrho = (lrhomax-lrhomin)/(ntot-1)
    table = open(filename,'w')
    table.write('# \n # \n # \n # \n # \n')
    table.write('%d    <-- Number of lines\n' % ntot)
    table.write('#\n#     n_B [fm^{-3}]  rho [g/cm^3]   p [dyn/cm^2]\n#\n')
    for i in range(ntot):
        rho_tmp = 10**(lrhomin + i*ldrho)
        nb = rho_tmp/(1.66e-24*1e39)
        k_tmp = kappa[0]
        gamma_tmp = gammas[0]
        eps0_tmp = eps0[0]
        for j,rho in enumerate(rhos):
            if rho_tmp > rho:
                k_tmp = kappa[j+1]
                gamma_tmp = gammas[j+1]
                eps0_tmp = eps0[j+1]
        e=rho_tmp*(1.0 + k_tmp/(gamma_tmp - 1.0)*rho_tmp**(gamma_tmp-1.0) + eps0_tmp)
        p=k_tmp*rho_tmp**gamma_tmp*(c**2*10000)
        table.write('    %d    %1.15e    %1.15e    %1.15e \n' % (i+1,nb,e,p))

# Builds a table for rns_id from piecewise polytropic data.
# Assumes parameters from Read et. al 2009

def pp2rns_crust(filename,hd_gamma,log10p1,ntot=200,rhomin=5e6,rhomax=3e15,mb=1.66e-24,verbose=False,crust=True):
    if ntot > 200:
        print ('WARNING: rns_id accepts only tables with maximum 200 entries by default!')
    p1 = 10**log10p1
    kappa = np.zeros(7)
    eps0 = np.zeros(7)
    eps0[0]=0
    #Setting up crust and free fermion gas
    if crust:
        kappa[0]=6.80110e-9
        kappa[1]=1.06186e-6
        kappa[2]=5.32697e1
        kappa[3]=3.99874e-8
        gammas=np.zeros(7)
        gammas[0]=1.58425
        gammas[1]=1.28733
        gammas[2]=0.62223
        gammas[3]=1.35692
    else:
        kappa[0]=3.99874e-8
        kappa[1]=3.99874e-8
        kappa[2]=3.99874e-8
        kappa[3]=3.99874e-8
        gammas=np.zeros(7)
        gammas[0]=1.35692
        gammas[1]=1.35692
        gammas[2]=1.35692
        gammas[3]=1.35692
    rhos = np.zeros(len(gammas)-1)
    rhos[0]=2.44034e7
    rhos[1]=3.78358e11
    rhos[2]=2.62780e12
    #Calculating rho between low and high density part
    gammas[4]=hd_gamma[0]
    gammas[5]=hd_gamma[1]
    gammas[6]=hd_gamma[2]
    rhos[4]=10**14.7
    rhos[5]=1e15
    kappa[5]=p1/(c**2*10000*rhos[4]**gammas[5])
    kappa[4]=p1/(c**2*10000*rhos[4]**(gammas[4]))
    rhos[3]=(kappa[3]/kappa[4])**(1/(gammas[4]-gammas[3]))
    kappa[6]=kappa[5]*rhos[5]**(gammas[5]-gammas[6])
    if verbose:
        print ('kappa',kappa)
        print ('rho',rhos)
        print ('gamma',gammas)
        print ('pl', [kappa[i]*rhos[i]**gammas[i] for i in range(6)])
        print ('pr', [kappa[i+1]*rhos[i]**gammas[i+1] for i in range(6)])
    for i,rho in enumerate(rhos):
        eps0[i+1]=eps0[i]+kappa[i]/(gammas[i]-1.0)*rho**(gammas[i]-1.0)- kappa[i+1]/(gammas[i+1]-1.0)*rho**(gammas[i+1]-1.0)
    lrhomin = np.log10(rhomin)
    lrhomax = np.log10(rhomax)
    ldrho = (lrhomax-lrhomin)/(ntot-1)
    table = open(filename,'w')
    table.write('%d \n' % ntot)
    for i in range(ntot):
        rho_tmp = 10**(lrhomin + i*ldrho)
        nb = rho_tmp/(1.66e-24*1e39)
        k_tmp = kappa[0]
        gamma_tmp = gammas[0]
        eps0_tmp = eps0[0]
        for j,rho in enumerate(rhos):
            if rho_tmp > rho:
                k_tmp = kappa[j+1]
                gamma_tmp = gammas[j+1]
                eps0_tmp = eps0[j+1]
        e=rho_tmp*(1.0 + k_tmp/(gamma_tmp - 1.0)*rho_tmp**(gamma_tmp-1.0) + eps0_tmp)
        p=k_tmp*rho_tmp**gamma_tmp
        h=np.log((e+p)/rho_tmp)
        p=p*(c**2*10000)
        h=h*(c**2*10000)
        nb=rho_tmp/mb
        table.write('    %1.15e    %1.15e    %1.15e    %1.15e \n' % (e,p,h,nb))

def Ferrara2rns(filename,quark=False,hadroneos='hadronic.out'):
    table = np.loadtxt(filename,unpack=True)
    name = os.path.split(filename)[1]
    print ('name1= ',name)
    name = os.path.splitext(name)[0]
    print ('name2= ',name)
    rns = open(name+'_e_vs_p','w')
    if quark:
        h_table=np.loadtxt(hadroneos,unpack=True)
        n = np.arange(table[2][0],h_table[2][-1],0.001)
        pq = np.interp(n,table[2],table[0])
        ph = np.interp(n,h_table[2],h_table[0])
        press = pq-ph
        ncrit = np.interp(0,press,n)
        icrit = np.searchsorted(h_table[2],ncrit)
        qcrit = np.searchsorted(table[2],ncrit)
        p = np.zeros(icrit+len(table[0])-qcrit)
        e = np.zeros(icrit+len(table[0])-qcrit)
        for i in range(icrit):
            p[i]=h_table[0][i]*CU_to_km**2*CU_to_densCGS
            e[i] = h_table[1][i]*CU_to_km**2*CU_to_densCGS
        for j in range(icrit,icrit+len(table[0])-qcrit):
            p[j]=table[0][j-icrit+qcrit]*CU_to_km**2*CU_to_densCGS
            e[j] = table[1][j-icrit+qcrit]*CU_to_km**2*CU_to_densCGS
    
    else:
        p = table[0]*CU_to_km**2*CU_to_densCGS
        e = table[1]*CU_to_km**2*CU_to_densCGS
    p = p*c**2*10000
    plt.plot(p)
    ntot = len(p)
    print (ntot)
    #rns.write('%d \n' %(ntot-22))
    for i in range(ntot):
        j=i
        rns.write('    %1.15e    %1.15e\n' % (e[j],p[j]))

##################################################################
##
##
##################################################################

##########################################
##
##
##
##
#########################################

def Build_LORENE_table(filename,eIN,pIN,nIN,units='CGS') :
    pass;
    #########################################################
    #  RNS table are construct in CGS UNITS
    #  e  = [g/cm**3] 
    #  p  = [g cm/s*2 / cm**2 =  g/cm**3 (cm/s)**2]
    #  n  = 1/cm**3
    #  h  = cCGS**2 * np.log((e+p/cCGS**2)/(nb * MbaryonCGS))
    #########################################################
    ntot = len(eIN) 
    ###
    table = open(filename,'w')
    table.write('# \n # \n # \n # \n # \n')
    table.write('%d    <-- Number of lines\n' % ntot)
    table.write('#\n#     n_B [fm^{-3}]  rho [g/cm^3]   p [dyn/cm^2]\n#\n')
    mb = 1.66e-24
    for i in range(ntot):
        nb = nIN[i]/mb/1e39
        e  = eIN[i]
        p  = pIN[i]
        table.write('    %d    %1.15e    %1.15e    %1.15e \n' % (i+1,nb,e,p))

##########################################
##
##
##
##
#########################################

def Build_RNS_table(filename,eIN,pIN,nIN,units='CGS') :


    if (units == 'Mev,fermi') :
        nIN *= 1.0/(fermiCGS**3)  
        eIN *= (1e6*eV*1e7) / (cCGS**2)  /(fermiCGS**3)  
        pIN *= (1e6*eV*1e7) / (fermiCGS**3)  

    #########################################################
    #  RNS table are construct in CGS UNITS
    #  e  = [g/cm**3] 
    #  p  = [g cm/s*2 / cm**2 =  g/cm**3 (cm/s)**2]
    #  n  = 1/cm**3
    #  h  = cCGS**2 * np.log((e+p/cCGS**2)/(nb * MbaryonCGS))
    #########################################################
    ntot = len(eIN) 
    table = open(filename,'w')
    table.write('%d \n' % ntot)
    for i in range(ntot):
        e   = eIN[i]
        p   = pIN[i]
        n   = nIN[i]
        rho = n * MbaryonCGS
        h   = cCGS**2 * np.log((e+p/cCGS**2)/rho)
        if h < 1 :
           h = 1.0
        table.write('    %1.15e    %1.15e    %1.15e    %1.15e \n' % (e,p,h,n))
