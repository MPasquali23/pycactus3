#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import scipy 
import scipy.signal 
import re

# License: Creative Commons Zero (almost public domain) http://scpyce.org/cc0



#######################################################                        
# Example of wave extraction:
# from CarpetH5 import *
# from UtilityAnalisys import * 
# psi4  = ReadScala('../RAWDATA/hdf5/Sly14vs14_r25.h5')['PSI4']
# t_ret = RetardedTime(psi4['700']['l2m2']['time'],r=700,M=2.5)
# h22   = NIntegratePsi4(psi4['700']['l2m-2'],Nint=2,r=700,M=2.5)
###########################################################
def Riso(r=0,M=0):
    if r == 0:
        return r
    else:
        if M > 1e-8:
            rA = r*(1+M/(2*r))**2
        else :
            rA = r
        return rA 

def RetardedTime(t,r=0,M=0):
    if r == 0:
        return t
    else:
        if M > 1e-8:
            rA = r*(1+M/(2*r))**2
            rstar = rA +2*M*np.log(rA/(2*M)-1)
        else :
            rstar = r
        return t - rstar 

def Derivative1(f,t,order=1) :
    OUT = 0.0 * np.array(f)
    if (order ==1 ) :
        OUT[1:] = (f[1:]-f[:-1])/(t[1:]-t[:-1])
    elif (order ==1 ) :
        OUT[1:-1] = (f[2:]-f[:-2])/(t[2:]-t[:-2])
    return OUT

def Integrate1(f,t) : return (scipy.integrate.cumtrapz(f,x=t,initial=0.0))
def Integrate2(f,t) : return (scipy.integrate.cumtrapz(Integrate1(x,t),x=t,initial=0.0))
def Integrate3(f,t) : return (scipy.integrate.cumtrapz(Integrate2(x,t),x=t,initial=0.0))
def NIntegratePsi4(psi4,Nint=2,r=0,M=0,order=2):
    num_integrals=Nint
    dt = (psi4['time'][1]-psi4['time'][0])
    t_ret = RetardedTime(psi4['time'],r,M)
    idx_t0 = np.searchsorted(t_ret,0)
    psic = psi4['re']+1j*psi4['im']
    integrand = psic
    for i in range(num_integrals):
        integrand[0:idx_t0]=0
        integrand[idx_t0:]=scipy.integrate.cumtrapz(integrand[idx_t0:],x=t_ret[idx_t0:],initial=0)
    h = integrand
    h_int_const=np.polyfit(t_ret[idx_t0:],h[idx_t0:],order)
    h[idx_t0:] = h[idx_t0:] - np.polyval(h_int_const,t_ret[idx_t0:])
    return h

#########################################################################
#Radiated energy without infinite radius extrapolation, from pyGWAnalysis                         
#########################################################################
def Radiated_Energy_dt(psi4s,r=700,M=0,order=1,modes=[],int_method='time',f0=0):
    psi4 = psi4s[str(r)]
    dEdt=np.zeros(len(psi4['l2m2']['re']))
    if len(modes) > 0:
        mode_list=list(set(modes).intersection(list(psi4)))
    else:
        mode_list = list(psi4)
    for mode in mode_list:
        ###print(mode)
        if int_method == 'time':
            int1 = NIntegratePsi4(psi4[mode],Nint=1,r=0,M=M,order=order)
        elif int_method == 'filter':
            int2 = NIntegratePsi4_filter(psi4[mode],f0,Nint=2,r=0,M=M)
            int1 = der4(int2,np.diff(psi4[mode]['time'])[1])
        dEdt=dEdt+1/(16*np.pi)*(r*np.abs(int1))**2
    return dEdt
###################################################################################
#Radiated AngularMomentum without infinite radius extrapolation, from pyGWAnalysis                
###################################################################################
def Radiated_AngularMomentum_dt(psi4s,r=700,M=0,lmax=2,order=1,int_method='time',f0=0):
    psi4 = psi4s[str(r)]
    dLdt_z=np.zeros(len(psi4['l2m2']['time']))
    dLdt_x=np.zeros(len(psi4['l2m2']['time']))
    dLdt_y=np.zeros(len(psi4['l2m2']['time']))
    for l in range(2,lmax+1):
        for m in range(-l,l+1):
            term_y = 0
            term_x = 0
            if m+1 < l:
                flm = np.sqrt(l*(l+1)-m*(m+1))
                mode = 'l'+str(l)+'m'+str(m+1)
                if int_method == 'time':
                    int_lmp = NIntegratePsi4(psi4[mode],Nint=1,r=0,M=M,order=order)
                elif int_method == 'filter':
                    int_lmp = np.conjugate(NIntegratePsi4_filter(psi4[mode],f0,Nint=1,r=0,M=M))
                ##########integrationN(psi4s[mode],M,0,order,num_integrals=1,conj_psi=True)
                term_x = term_x + r*int_lmp*flm
                term_y = term_y + r*int_lmp*flm
            if m-1 > -l:
                flmm = np.sqrt(l*(l+1)+m*(1-m))
                mode = 'l'+str(l)+'m'+str(m-1)
                if int_method == 'time':
                    int_lmm = NIntegratePsi4(psi4[mode],Nint=1,r=0,M=M,order=order)
                elif int_method == 'filter':
                    int_lmm = np.conjugate(NIntegratePsi4_filter(psi4[mode],f0,Nint=1,r=0,M=M))
                ##########integrationN(psi4s[mode],M,0,order,num_integrals=1,conj_psi4s=True)
                term_x = term_x + r*int_lmm*flmm
                term_y = term_y - r*int_lmm*flmm
            mode = 'l'+str(l)+'m'+str(m)
            if int_method == 'time':
                int_int_lm = NIntegratePsi4(psi4[mode],Nint=2,r=0,M=M,order=order)
            elif int_method == 'filter':
                int_int_lm = np.conjugate(NIntegratePsi4_filter(psi4[mode],f0,Nint=2,r=0,M=M))
            ##########integrationN(psi4s[mode],M,0,order+1,num_integrals=2,conj_psi4s=False)
            if int_method == 'time':
                int_lm     = np.conjugate(NIntegratePsi4(psi4[mode],Nint=1,r=0,M=M,order=order))
            elif int_method == 'filter':
                int_lm     = NIntegratePsi4_filter(psi4[mode],f0,Nint=1,r=0,M=M)
            #############integrationN(psi4s[mode],M,0,order,num_integrals=1,conj_psi4s=True)
            dLdt_z = dLdt_z + 1/(16*np.pi)*m*r**2*int_int_lm*int_lm
            dLdt_x = dLdt_x + 1/(32*np.pi)*int_int_lm*r*term_x
            dLdt_y = dLdt_y - 1/(32*np.pi)*int_int_lm*r*term_y
    dLdt = np.array([np.imag(dLdt_x),np.real(dLdt_y),np.imag(dLdt_z)])
    dLdt = (np.imag(dLdt_x),np.real(dLdt_y),np.imag(dLdt_z))
    return dLdt

#####################################################
## FORMULA form http://arxiv.org/pdf/0707.4654v3.pdf
##
## I do think the formula is correct.
## ** DO NOT UNDERSTAND WHY DO NOT WORKs
##
#####################################################
def coefAlm(l,m) :
    return np.sqrt(1.0*((l-m)*(l+m+1))) / (l*(l+1))
def coefBlm(l,m) :
    return  1.0/(2*l)*np.sqrt( 1.0*((l-2)*(l+2)*(l+m)*(l+m-1))/((2*l-1)*(2*l+1)) )
def coefClm(l,m) :
    return 2.0 * m / (l*(l+1))
def coefDlm(l,m) :
    return  1.0/(l)*np.sqrt( 1.0*((l-2)*(l+2)*(l-m)*(l+m))/((2*l-1)*(2*l+1)) )

def Radiated_LinearMomentum_dt(psi4s,r=700,M=0,lmax=3,order=1):
    psi = psi4s[str(r)]
    dPxy = np.zeros(len(psi['l2m2']['time']))
    dPz  = np.zeros(len(psi['l2m2']['time']))
    dPxy = dPxy.astype(complex)
    for l in range(2,lmax+1):
        for m in range(-l,l+1):
            term_conj_xy = np.zeros(len(psi['l2m2']['time']))
            term_conj_z  = np.zeros(len(psi['l2m2']['time']))
            term_conj_xy = term_conj_xy.astype(complex)
            term_conj_z  = term_conj_z.astype(complex)
            mode = 'l'+str(l)+'m'+str(m)
            int0 = NIntegratePsi4(psi[mode],Nint=1,r=0,M=M,order=order)
            ##-----------------------------------------------------------
            ## Formula for dPz  Hdot[l,m] *
            ##              ( Clm[l,m]  barHdot[l,m] 
            ##                +Dlm[l,m] barHdot[l-1,m] + Dlm[l+1,m] barHdot[l+1,m]
            ##              )
            ##-----------------------------------------------------------
            barHdot = np.conj(NIntegratePsi4(psi[mode],Nint=1,r=0,M=M,order=order))   
            term_conj_z  = term_conj_z + coefClm(l,m) * barHdot
            if (( l-1 >= 2 ) & ( l-1 >= np.abs(m) ) ) : 
                Xmode = 'l'+str(l-1)+'m'+str(m)
                barHdot = np.conj(NIntegratePsi4(psi[Xmode],Nint=1,r=0,M=M,order=order))   
                term_conj_z  = term_conj_z + coefDlm(l,m) * barHdot
            if ( l+1 <= lmax ) : 
                Xmode = 'l'+str(l+1)+'m'+str(m)
                barHdot = np.conj(NIntegratePsi4(psi[Xmode],Nint=1,r=0,M=M,order=order))   
                term_conj_z  = term_conj_z + coefDlm(l+1,m) * barHdot
            ##-----------------------------------------------------------
            ## Formula for dPxy  Hdot[l,m] *
            ##              ( Alm[l,m]  barHdot[l,m+1] 
            ##              + Blm[l,-m] barHdot[l-1,m+1] - Blm[l+1,m+1] barHdot[l+1,m+1]
            ##              )
            ##-----------------------------------------------------------
            if ( m+1 <= l ) : 
                Xmode = 'l'+str(l)+'m'+str(m+1)
                barHdot = np.conj(NIntegratePsi4(psi[Xmode],Nint=1,r=0,M=M,order=order))   
                term_conj_xy  = term_conj_xy + coefAlm(l,m) * barHdot
            if ( (m+1 <= l-1 ) & (m+1 >= -l ) & (l-1 >=2 ) ) : 
                Xmode = 'l'+str(l-1)+'m'+str(m+1)
                barHdot = np.conj(NIntegratePsi4(psi[Xmode],Nint=1,r=0,M=M,order=order))   
                term_conj_xy  = term_conj_xy + coefBlm(l,-m) * barHdot
            if ( (m+1 <= l+1 ) & ( l+1 < lmax ) ) : 
                Xmode = 'l'+str(l+1)+'m'+str(m+1)
                barHdot = np.conj(NIntegratePsi4(psi[Xmode],Nint=1,r=0,M=M,order=order))   
                term_conj_xy  = term_conj_xy - coefBlm(l+1,m) * barHdot
            ###########################################
            ##  NORMALIZE
            ###########################################
            dPxy = dPxy + int0*term_conj_xy
            dPz  = dPz  + int0*term_conj_z
    dPx = np.real(r**2.0/(8*np.pi)*dPxy)
    dPy = np.imag(r**2.0/(8*np.pi)*dPxy)
    dPz = np.real(r**2.0/(8*np.pi)*dPz)
    return (dPx,dPy,dPz)


##############################################################
## dd=H5.ReadScalarHDF5('./WorkFile/hdf5/Sly14vs14_r25.h5')
## time=dd['PSI4']['700']['l2m2']['time']
## xx=NIntegrateAllPsi4(dd['PSI4']['700'],Nint=2,order=1)
## Px,Py,Pz=Radiated_LinearMomentum_dt_fromH(xx,time,700)
##############################################################
def Radiated_LinearMomentum_dt_fromH(allH,time,r=700,M=0,lmax=3,order=1):
    dotH = TimeDerivative_ModesData(allH,time)
    dPxy = np.zeros(len(time))
    dPz  = np.zeros(len(time))
    dPxy = dPxy.astype(complex)
    for l in range(2,lmax+1):
        for m in range(-l,l+1):
            term_conj_xy = np.zeros(len(time))
            term_conj_z  = np.zeros(len(time))
            term_conj_xy = term_conj_xy.astype(complex)
            term_conj_z  = term_conj_z.astype(complex)
            mode = 'l'+str(l)+'m'+str(m)
            int0 = dotH[mode]
            ##-----------------------------------------------------------
            ## Formula for dPz  Hdot[l,m] *
            ##              ( Clm[l,m]  barHdot[l,m] 
            ##                +Dlm[l,m] barHdot[l-1,m] + Dlm[l+1,m] barHdot[l+1,m]
            ##              )
            ##-----------------------------------------------------------
            barHdot = np.conj(dotH[mode])
            term_conj_z  = term_conj_z + coefClm(l,m) * barHdot
            if (( l-1 >= 2 ) & ( l-1 >= np.abs(m) ) ) : 
                Xmode = 'l'+str(l-1)+'m'+str(m)
                barHdot = np.conj(dotH[Xmode])
                term_conj_z  = term_conj_z + coefDlm(l,m) * barHdot
            if ( l+1 <= lmax ) : 
                Xmode = 'l'+str(l+1)+'m'+str(m)
                barHdot = np.conj(dotH[Xmode])
                term_conj_z  = term_conj_z + coefDlm(l+1,m) * barHdot
            ##-----------------------------------------------------------
            ## Formula for dPxy  Hdot[l,m] *
            ##              ( Alm[l,m]  barHdot[l,m+1] 
            ##              + Blm[l,-m] barHdot[l-1,m+1] - Blm[l+1,m+1] barHdot[l+1,m+1]
            ##              )
            ##-----------------------------------------------------------
            if ( m+1 <= l ) : 
                Xmode = 'l'+str(l)+'m'+str(m+1)
                barHdot = np.conj(dotH[Xmode])
                term_conj_xy  = term_conj_xy + coefAlm(l,m) * barHdot
            if ( (m+1 <= l-1 ) & (m+1 >= -l ) & (l-1 >=2 ) ) : 
                Xmode = 'l'+str(l-1)+'m'+str(m+1)
                barHdot = np.conj(dotH[Xmode])
                term_conj_xy  = term_conj_xy + coefBlm(l,-m) * barHdot
            if ( (m+1 <= l+1 ) & ( l+1 < lmax ) ) : 
                Xmode = 'l'+str(l+1)+'m'+str(m+1)
                barHdot = np.conj(dotH[Xmode])
                term_conj_xy  = term_conj_xy - coefBlm(l+1,m) * barHdot
            ###########################################
            ##  NORMALIZE
            ###########################################
            dPxy = dPxy + int0*term_conj_xy
            dPz  = dPz  + int0*term_conj_z
    dPx = np.real(r**2.0/(8*np.pi)*dPxy)
    dPy = np.imag(r**2.0/(8*np.pi)*dPxy)
    dPz = np.real(r**2.0/(8*np.pi)*dPz)
    return (dPx,dPy,dPz)



###################################################################################################
##
##
##
##    UTILITY TO WORK WITH MODE EXPANSION
##
##
##
###################################################################################################

RE_match_modes ='l(\d)m(\d*)'
RE_match_modesC=re.compile('l(\d+)m([-+]?\d+)')
def ModeName(l,m) : return ( 'l%dm%d' % (l,m) )
def ModesData_lmax(modes_data) :
    keylist = list(modes_data)
    l_min = 100 
    l_max = 0
    for i in keylist:
        x=RE_match_modesC.match(i) 
        if x :
            l = np.int(x.groups()[0])
            m = np.int(x.groups()[1])
            ##print i, 'l=%d m=%d' % (l,m)
            if l < l_min : l_min = l 
            if l > l_max : l_max = l 
        else :
            print ("Key ", i, " is not a mode")
    ##return (l_min,l_max)
    return l_max

def NIntegrateAllPsi4(psi4det,Nint=2,r=0,M=0,order=2,int_method='time',f0=0):
    keylist = list(psi4det)
    modes=filter(RE_match_modesC.match,keylist)
    OUT=dict()
    for i in modes: 
        ###print i
        if int_method=='time':
            OUT[i] = NIntegratePsi4(psi4det[i],Nint,r,M,order)
        elif int_method == 'filter':
            OUT[i] = NIntegratePsi4_filter(psi4det[i],f0,Nint,r,M)
        else:
            print ('Integration method not recognized!')
            return psi4det
    pass;
    return OUT

def TimeDerivative_ModesData(modes_data,time) :
    keylist = list(modes_data)
    modes=filter(RE_match_modesC.match,keylist)
    OUT=dict()
    lenOUT  =  len(modes_data[modes[0]])
    lentime =  len(time)
    if lentime != lenOUT :
        print ("Modes and time should be of the same size")
        return OUT
    for i in modes: 
        ##print i
        OUT[i] = Derivative1(modes_data[i],time,order=1)
    return OUT

def Reconstruct_From_ModesData(modes_data,thetaIN,phiIN,lmaxIN=20) :
     theta = 1.0 * np.array(thetaIN)
     phi   = 1.0 * np.array(phiIN)
     modes=filter(RE_match_modesC.match,modes_data)
     len_modes =  modes_data[modes[0]].shape

     OUT   = np.zeros(len_modes+phi.shape,dtype=complex128)
     print (OUT.shape)
     if theta.shape != phi.shape :
         return 0
     lmax = ModesData_lmax(modes_data)
     if lmaxIN < lmax :
         lmax= lmaxIN
     for l in range(2,lmax+1) :
         print ("Summing mode ",l)
         for m in range(-l,l+1) :
             modeSTR = ModeName(l,m) 
             valYslm = Yslm(-2,l,m,theta,phi)
             for k in range(len_modes[0]):
                 pass;
                 OUT[k] += modes_data[modeSTR][k]*valYslm
     return OUT

###################################################################################################
##
##  Spin-weighted spherical harmonics   Yslm(s,l,m,theta,phi)
##
##  main reference = https://en.wikipedia.org/wiki/Spin-weighted_spherical_harmonics
##
##
###################################################################################################

def Yslm(s,l,m,INtheta,INphi) :
    exp_i_half_theta  = np.exp(0.5j*np.array(INtheta))
    expphi = np.exp(1.0j * m * np.array(INphi))
    cos_half_theta = np.real(exp_i_half_theta)
    sin_half_theta = np.imag(exp_i_half_theta)

    NORM = (-1)**m *np.sqrt( ( 1.0*np.math.factorial(l-m)*np.math.factorial(l+m)*(2*l+1) )
                            /( 4*np.pi* np.math.factorial(l+s) * np.math.factorial(l-s)  ) )
    OUT  = np.zeros(expphi.shape)
    for i in range( max(0,m-s) , min(l+m,l-s)+1  ) :
        OUT += (  (-1)**(l-i-s) 
               * ( (np.math.factorial(l-s))/(np.math.factorial(i)*np.math.factorial(l-s-i))     ) 
               * ( (np.math.factorial(l+s))/(np.math.factorial(i+s-m)*np.math.factorial(l+m-i)) )
               * cos_half_theta**(2*i+s-m)
               * sin_half_theta**(2*(l-i)+m-s)
               )
    return  (NORM * expphi * OUT )

## Its complex conjugated
def barYslm(s,l,m,INtheta,INphi) :  return (-1)**(s + m) * Yslm(-s,l,-m,INtheta,INphi)

def SimpsonSperical(f,theta,phi) :
    if ( f.shape != theta.shape ) or ( f.shape != phi.shape) :
        print ("Argument do not have the same shape",f.shape,theta.shape,phi.shape)
        return 0
    if np.max((np.array(theta.shape) - 1)%2) != 0 :
        print ("Simpson integration requires odd dimensions !", theta.shape)
        return 0
    dS=np.diff(theta[0,:2])*np.diff(phi[:2,0])*np.sin(theta)
    return scipy.integrate.simps(scipy.integrate.simps(f*dS))


def SimpsonThetaPhiGrid(Ntheta=20,Nphi=0) :
    if Nphi == 0 :
        Nphi = Ntheta*2
    return np.meshgrid(np.linspace(0,np.pi,2*Ntheta+1,endpoint=True),np.linspace(0,2*np.pi,2*Nphi+1,endpoint=True))

# theta,phi = np.meshgrid(np.linspace(0,np.pi,51,endpoint=True),np.linspace(0,2*np.pi,51,endpoint=True))
##---------------------------------------------------------------------------------------------
# theta,phi = np.meshgrid(np.linspace(0,np.pi,51,endpoint=True),np.linspace(0,2*np.pi,51,endpoint=True))
# theta,phi = SimpsonThetaPhiGrid(Npoints=20)
# np.abs(SimpsonSperical(Yslm(-2,2,0,theta,phi)*np.conjugate(Yslm(-2,3,1,theta,phi)),theta,phi))
##---------------------------------------------------------------------------------------------

###################################################################################################
##
##
##
##
###################################################################################################

def spectrum(t,x,kind='fft',freq=np.array([1.0]),window=0,
             norm_in='none',norm_out=False,
             maxima=False,showfreq=False,fftshift=False,padding=1) :
    ''' The spectrum function compute the fourier trasform of a signal t,x(t) and 
    is called as h=spectrum(t,x).  By default it compute the standard "fft" of 
    the signal.

    OPTIONS: kind    = 'fft','int'',lomb'      [default=fft]
             norm_in = 'none','mean','linfit'  [subtract to x the mean or the linear term]
             freq    = np.array([...]) frequncy to use to calculate the 'lomb'
                       or the integral trasformation.
             window  = 0,1,2,3,4 [windowing function 0=none,1=blackman,2=bartlett
                                  3=hamming,4=hanning]
    FLAGS:   norm_out = 'False' [normalize the output to be max(abs(h))=1]
             maxima   = 'False' [Output the ordered maximum of (f,h)]
             showfreq = 'False' [Output (f,h) instead of just h]
             fftshift = 'False' [do fftshift reordering require kind='fft']
    '''
    #####################################
    ## Take out the mean value from FFT
    #####################################
    if (norm_in=='mean') :
       signal = x/np.mean(x)
    if (norm_in=='linfit') :
       poly =np.polyfit(t,x,1)
       poly1=np.poly1d(poly)
       signal = x -poly1(t) 
    else :
       signal = np.array(x)

    LEN = len(signal)
    ####################################
    ## Windowing function:
    ## (0)  None
    ## (1)  np.blackman
    ## (2)  np.bartlett
    ## (3)  np.hamming
    ## (4)  np.hanning
    ####################################
    if   (window == 1 ) :  signal*=scipy.signal.blackman(LEN)
    elif (window == 2 ) :  signal*=scipy.signal.bartlett(LEN)
    elif (window == 3 ) :  signal*=scipy.signal.hamming(LEN)
    elif (window == 4 ) :  signal*=scipy.signal.hanning(LEN)

    if kind == 'fft' :
        dt = t[1] -t[0]
        T  = t[-1]-t[0]
        ### f = 1/T * np.arange(0,LEN,dtype=np.float64)
        if fftshift == False :
            f = np.fft.fftfreq(t.shape[0]*padding,dt)
            h = (T/LEN)* np.fft.fft(signal,n=padding*LEN)
        else :
            f = np.fft.fftshift( np.fft.fftfreq(t.shape[0]*padding,dt) )
            h = np.fft.fftshift( (T/LEN)* np.fft.fft(signal,n=padding*LEN)   )
    elif kind == 'lomb':
        f=freq
        if np.any(np.iscomplex(signal)) :
            h=scipy.signal.lombscargle(t,np.real(signal),freq)
        else:
            h=scipy.signal.lombscargle(t,signal,freq)
    elif kind == 'int':
        f=freq
        h=np.array([np.trapz(signal*np.exp(-1j*2*np.pi*ff*t),t) for ff in freq])

    if norm_out== True :
        h=h/np.max(np.abs(h))
    if maxima == True :
        data = abs(h)
        c    = (np.diff(np.sign(np.diff(data))) < 0).nonzero()[0] + 1 # local max
        idx  = np.argsort(abs(h[c]))
        return (f[c[idx[::-1]]],h[c[idx[::-1]]])
    elif showfreq == False :
        return h
    else:        
        return (f,h)



###################################################################################################
##
##
##
##
##
##
##
###################################################################################################
                        

def FindMaximumTimeSeries(x,f,xi=0.0,xf=0.0) :
    if ( np.iscomplexobj(f) == True ) :
        fval = np.abs(f)
    else :
        fval = np.array(f)
    if (xi==xf) :
        xi = np.min(x)
        xf = np.max(x)
    idx = np.argmax(fval)
    ## -----------------------------------------------------
    ##  Do not understand why it does not work
    ## -----------------------------------------------------
    ## idx0 = max(0,idx-3)
    ## f    = scipy.interpolate.interp1d(x[idx0:(idx0+7)],fval[idx0:(idx0+7)],kind='cubic',fill_value=0.0) 
    ## xmin = scipy.optimize.fmin(lambda x: - 200*f(x),x[idx],disp=True)
    ## fmax = f(xmin)
    ## return (xmin,fmax,x[idx],fval[idx],idx)
    return (x[idx],fval[idx],idx)

def FindMaximumTimeSeries2(x,f,xi=0.0,xf=0.0) :
    tm,xx,idx = FindMaximumTimeSeries(x,f,xi,xf)
    while (np.abs(f[idx]) > np.abs(f[idx+1])):
        idx += 1

    return (x[idx],f[idx],idx)

###################################################################################################
##
##
##
##
##
##
##
###################################################################################################
                        


################################################################
####
################################################################


#### ----------------- PIECE WISE POLITRIPOYC EOS

def computes_ks_EOS_PieceWiseEOS(EOS) :
    n_pieces = EOS['n_pieces']
    EOS['k'  ] = np.zeros(n_pieces)
    EOS['eps'] = np.zeros(n_pieces)
    EOS['k'  ][0]  = EOS['k0']
    EOS['eps'][0]  = 0.0
    for i in range(n_pieces-1) :
        EOS['k'  ][i+1] = EOS['k'][i] * EOS['rho'][i]**(EOS['gamma'][i]-EOS['gamma'][i+1])
        EOS['eps'][i+1] = ( EOS['eps'][i] + 
                            EOS['k'][i]  * EOS['rho'][i]**(EOS['gamma'][i]-1.0) / (EOS['gamma'][i]-1.0) -
                            EOS['k'][i+1]* EOS['rho'][i]**(EOS['gamma'][i+1]-1.0) / (EOS['gamma'][i+1]-1.0)
                          )

def EOS_PieceWiseEOS(rho,eps,eosname='SLy',option="") :
    pass
    EOSs = dict()
    EOSs['SFHo'] = {
         'gamma_th' : 1.8 , 'n_pieces' : 10 ,
         'k0'    :  0.913387,
         'rho'   :  np.array([ 2.0e-9 , 7.0e-7   , 7.5e-6    , 2.0e-4 , 7.0e-4, 0.00114961 , 0.00169623 , 0.0027, 0.0054])  ,
         'gamma' :  np.array([
      1.37745
    , 1.2564
    , 0.602339   
    , 1.54708    
    , 2.62132   
    , 3.27854    
    , 2.91578   
    , 2.65451 
    , 2.51257
    , 1.50169  
    ])  ,
    }
    EOSs['DP'] = {
         'gamma_th' : 1.8 , 'n_pieces' : 10 ,
         'k0'    :  0.913387,
         'rho'   :  np.array([  2.0e-9, 7.0e-7     , 7.5e-6      , 2.2e-4, 8.8e-4  , 0.0013, 0.0018, 0.0026 , 0.005])  ,
         'gamma' :  np.array([  1.37737  , 1.25665, 0.594309 , 1.55798, 2.7214 , 1.95137 , 3.17436      , 2.24939    , 1.04838    , 1.41862    ])  ,
    }
    EOSs['SLy'] = {
         'gamma_th' : 1.8 , 'n_pieces' : 7 ,
         'k0'    :  1.685819e2,
         'rho'   :  np.array([               3.951156e-11, 6.125960e-07, 4.254672e-06, 2.367449e-04, 8.114721e-04, 1.619100e-03]),
         'gamma' :  np.array([ 1.58425     , 1.28733     , 0.62223     , 1.35692     , 3.005       , 2.988       , 2.851]),
    }
    EOSs['APR4'] = {
         'gamma_th' : 1.8 , 'n_pieces' : 7 ,
         'k0'    :  1.685819e2,
         'rho'   :  np.array([               3.951156e-11, 6.125960e-07, 4.254672e-06, 2.44789e-04 , 8.114721e-04, 1.619100e-03]),
         'gamma' :  np.array([ 1.58425     , 1.28733     , 0.62223     , 1.35692     , 2.83        , 3.445       , 3.348]),
    }
    EOSs['ALF2'] = {
         'gamma_th' : 1.8 , 'n_pieces' : 7 ,
         'k0'    :  1.685819e2,
         'rho'   :  np.array([               3.951156e-11, 6.125960e-07, 4.254672e-06, 3.153382e-04, 8.114721e-04, 1.619100e-03]),
         'gamma' :  np.array([ 1.58425     , 1.28733     , 0.62223     , 1.35692     , 4.07        , 2.411       , 1.98]),
    }
    EOSs['MS1'] = {
         'gamma_th' : 1.8 , 'n_pieces' : 7 ,
         'k0'    :  1.685819e2,
         'rho'   :  np.array([               3.951156e-11, 6.125960e-07, 4.254672e-06, 1.52466e-04, 8.114721e-04, 1.619100e-03]),
         'gamma' :  np.array([ 1.58425     , 1.28733     , 0.62223     , 1.35692     , 3.224       , 3.033       , 1.325]),
    }
    EOSs['H4'] = {
         'gamma_th' : 1.8 , 'n_pieces' : 7 ,
         'k0'    :  1.685819e2,
         'rho'   :  np.array([               3.951156e-11, 6.125960e-07, 4.254672e-06, 1.43736e-04 , 8.114721e-04, 1.619100e-03]),
         'gamma' :  np.array([ 1.58425     , 1.28733     , 0.62223     , 1.35692     , 2.909       , 2.246       , 2.144]),
    }
    EOSs['G275'] = {
         'gamma_th' : 1.8 , 'n_pieces' : 1 ,
         'k0'    :  30000.0,
         'rho'   :  [],
         'gamma' :  np.array([ 2.75       ])
    }
    EOS = EOSs[eosname]
    computes_ks_EOS_PieceWiseEOS(EOS)
    npieces=EOS['n_pieces']
    if (npieces <2 ) :
        tempK     = EOS['k'][0] *np.ones(rho.shape)  
        tempGamma = EOS['gamma'][0] * np.ones(rho.shape)
    else:
        tempK     = np.zeros(rho.shape)  
        tempGamma = np.zeros(rho.shape)
        tempEps   = np.zeros(rho.shape)
        tempEps   = np.zeros(rho.shape)
        tempK     += (EOS['k'][0]     * (( rho < EOS['rho'][0])) ) 
        tempGamma += (EOS['gamma'][0] * (( rho < EOS['rho'][0])) ) 
        tempEps   += (EOS['eps'][0]   * (( rho < EOS['rho'][0])) ) 
        for i in range(1,npieces-1) :
            tempK     += (EOS['k'][i]     * ( ( rho >= EOS['rho'][i-1])*( rho < EOS['rho'][i])) ) 
            tempGamma += (EOS['gamma'][i] * ( ( rho >= EOS['rho'][i-1])*( rho < EOS['rho'][i])) ) 
            tempEps   += (EOS['eps'][i]   * ( ( rho >= EOS['rho'][i-1])*( rho < EOS['rho'][i])) ) 
            pass;
        tempK     += (EOS['k'][npieces-1]     *(( rho >= EOS['rho'][npieces-2])) ) 
        tempGamma += (EOS['gamma'][npieces-1] *(( rho >= EOS['rho'][npieces-2])) ) 
        tempEps   += (EOS['eps'][npieces-1]   *(( rho >= EOS['rho'][npieces-2])) ) 
         
    Ppoly = tempK * rho**tempGamma
    Epoly = (tempK/(tempGamma-1.0)) * rho**(tempGamma-1.0) + tempEps
    Eterm= eps - Epoly
    P    = Ppoly + (EOS['gamma_th']-1) * rho * (Eterm * (Eterm >= 0.0))

    ############   == return Gamma1 =====
    if  option== "Gamma1" :
        return  (   EOS['gamma_th'] 
                 + (tempGamma-EOS['gamma_th'])*Ppoly/P 
                )
    ############   == return Gamma1 =====
    if np.max(eps) > 0.0 :
       return Eterm,P
    else:
       return Epoly,Ppoly

###-------------------------------------------------------------------------------------------------
##      hybrid_k(1) = hybrid_k0
##      hybrid_eps(1) = 0
##      if (n_pieces .gt. 1) then
##         do p = 1,n_pieces-1
##            hybrid_k(p+1) = hybrid_k(p) * hybrid_rho(p)**(hybrid_gamma(p)-hybrid_gamma(p+1))
##            hybrid_eps(p+1) = hybrid_eps(p) + &
##                 hybrid_k(p)*hybrid_rho(p)**(hybrid_gamma(p)-1.0d0)/(hybrid_gamma(p)-1.0d0) - &
##                 hybrid_k(p+1) * hybrid_rho(p)**(hybrid_gamma(p+1)-1.0d0)/(hybrid_gamma(p+1)-1.0d0) 
##         end do
### ---to be completed
#do i=1,npoints
#   hybrid_local_gamma = hybrid_gamma(1)
#   hybrid_local_k = hybrid_k(1)
#   hybrid_local_eps = hybrid_eps(1)
#   if (n_pieces .gt. 1) then
#      do p = 1,n_pieces-1
#         if (rho(i) .gt. hybrid_rho(p)) then
#            hybrid_local_gamma = hybrid_gamma(p+1)
#            hybrid_local_k   = hybrid_k(p+1)
#            hybrid_local_eps = hybrid_eps(p+1)
#         end if
#      end do
#   end if
#   if (keytemp .eq. 1) then
#      !Set epsilon to the cold part only
#      eps(i)=hybrid_local_k/(hybrid_local_gamma - 1.0)*rho(i)**(hybrid_local_gamma - 1.0) + hybrid_local_eps
#   end if
#   hybrid_p_poly = hybrid_local_k * (rho(i))**hybrid_local_gamma
#   hybrid_p_th = (hybrid_gamma_th-1)* 
#        (rho(i)*eps(i) - &
#         hybrid_p_poly/(hybrid_local_gamma-1) - &
#         hybrid_local_eps * rho(i))
#   hybrid_p_th = max(zero, hybrid_p_th)
##      hybrid_k(1) = hybrid_k0
##      hybrid_eps(1) = 0
##      if (n_pieces .gt. 1) then
##         do p = 1,n_pieces-1
##            hybrid_k(p+1) = hybrid_k(p) * hybrid_rho(p)**(hybrid_gamma(p)-hybrid_gamma(p+1))
##            hybrid_eps(p+1) = hybrid_eps(p) + &
##                 hybrid_k(p)*hybrid_rho(p)**(hybrid_gamma(p)-1.0d0)/(hybrid_gamma(p)-1.0d0) - &
##                 hybrid_k(p+1) * hybrid_rho(p)**(hybrid_gamma(p+1)-1.0d0)/(hybrid_gamma(p+1)-1.0d0) 
##         end do


#####################################################################
# Align waveforms
#####################################################################

# Align h1(t) to h2(t) allowing for a time dilatation (par.x[0]) 
# and a frequency shift (par.x[1])

def AlignAllWaveform(h1,h2,time,t1,t2):
    idx1 = np.searchsorted(time,time[0]+t1)
    idx2 = np.searchsorted(time,time[0]+t2)
    A1=np.abs(h1)
    A2=np.abs(h2)
    phi1=np.angle(h1)
    phi2=np.angle(h2)
    ChiSquare = lambda x: scipy.integrate.simps(np.abs(np.interp(time[idx1:idx2]*x[0],time,A1)*np.exp(1j*x[0]*np.interp(time[idx1:idx2]*x[0],time,phi1)+1j*x[1])-A2[idx1:idx2]*np.exp(1j*phi2[idx1:idx2]))**2)
    par=scipy.optimize.minimize(ChiSquare,[1,0],bounds=((0.5,1.5),(-np.pi,np.pi)),method="SLSQP")
    h_aligned = np.interp(time*par.x[0],time,A1)*np.exp(1j*par.x[0]*np.interp(time*par.x[0],time,phi1)+1j*par.x[1])
    return h_aligned,par.x[0],par.x[1]

# Align waveform phase phi1(t) to phi2(t) allowing for a time 
# and a frequency shift.

def AlignWavesPhase(phi1,phi2,time,t1,t2):
    idx1 = np.searchsorted(time,time[0]+t1)
    idx2 = np.searchsorted(time,time[0]+t2)
    ChiSquaredPhi = lambda x: scipy.integrate.simps((phi2[idx1:idx2]-np.interp(time[idx1:idx2]-x[0],time,phi1)-x[1])**2,x=time[idx1:idx2])
    #par=scipy.optimize.minimize(ChiSquaredPhi,[0,0],bounds=((-50,50),(-3,3)),method="SLSQP")
    par=scipy.optimize.minimize(ChiSquaredPhi,[0,0],method="Nelder-Mead")
    phi_aligned = np.interp(time-par.x[0],time,phi1+par.x[1])
    return par.x[0],par.x[1],phi_aligned

def AlignWavesPhase_not(phi1,phi2,time,t1,t2):
    idx1 = np.searchsorted(time,time[0]+t1)
    idx2 = np.searchsorted(time,time[0]+t2)
    ChiSquaredPhi = lambda x: scipy.integrate.simps((phi2[idx1:idx2]-phi1[idx1:idx2]-x[0])**2,x=time[idx1:idx2])
    #par=scipy.optimize.minimize(ChiSquaredPhi,[0,0],bounds=((-50,50),(-3,3)),method="SLSQP")
    par=scipy.optimize.minimize(ChiSquaredPhi,[0],method="Nelder-Mead")
    phi_aligned = phi1+par.x[0]
    return par.x[0],phi_aligned


# Align waveform phase phi1(t) to phi2(t) allowing for a time DILATATION 
# and a frequency shift.

def AlignWavesPhase_v2(phi1,phi2,time,t1,t2):
    idx1 = np.searchsorted(time,time[0]+t1)
    idx2 = np.searchsorted(time,time[0]+t2)
    ChiSquaredPhi = lambda x: scipy.integrate.simps((phi2[idx1:idx2]-np.interp(time[idx1:idx2]*x[0],time,phi1)-x[1])**2,x=time[idx1:idx2])
    #par=scipy.optimize.minimize(ChiSquaredPhi,[0,0],bounds=((-50,50),(-3,3)),method="SLSQP")
    par=scipy.optimize.minimize(ChiSquaredPhi,[1,0],method="Nelder-Mead")
    phi_aligned = np.interp(time*par.x[0],time,phi1+par.x[1])
    return par.x[0],par.x[1],phi_aligned

###########################################################
# Psi4 extrapolation to spacial infinity.
# 
##########################################################

# First order extrapolation
# Let you select the integration method: "time" -> NIntegratePsi4
# "ffi" -> NIntegratePsi4_ffi
# "filter" -> NIntegratePsi4_filter
# from Nakano et al. PRD 91,104022

def first_order_extrapolation(psi4_in,R,M,l=2,m=2,order=1,int_method='time',f0=0):
    psi4NR = psi4_in['re']+1j*psi4_in['im']
    r = R*(1+M/(2*R))**2
    if (int_method == 'time'):
        psi4_int = NIntegratePsi4(psi4_in,1,0,M,order=order)
    elif (int_method == 'ffi'):
        dt = psi4_in['time'][1]-psi4_in['time'][0]
        idx200 = np.searchsorted(RetardedTime(psi4_in['time'],r,M),200)
        idx400 = np.searchsorted(RetardedTime(psi4_in['time'],r,M),400)
        domega = np.diff(np.unwrap(-np.angle(psi4NR)))/dt
        f0_a = np.min(np.abs(domega[idx200:idx400]))
        f0 = np.abs(f0_a*m/(4*np.pi))
        psi4_int = NIntegratePsi4_ffi(psi4NR,dt,f0,num_integrals=1)
    elif (int_method == 'filter'):
        psi4_int = np.conj(NIntegratePsi4_filter(psi4_in,f0,1,0,M))
    else:
        print ("Integration method not recongnized!")
        return;
    psi_inf = (1-2*M/r)*(psi4NR - (l-1)*(l+2)/(2*r)*psi4_int)
    psi4_out=dict()
    psi4_out['re']=np.real(psi_inf)
    psi4_out['im']=np.imag(psi_inf)
    psi4_out['time']=psi4_in['time']
    return psi4_out

# Second order extrapolation (without the Kerr part)
# Let you select the integration method: "time" -> NIntegratePsi4
# "ffi" -> NIntegratePsi4_ffi
# "filter" -> NIntegratePsi4_filter
# Uses diff(diff(h)) instead of psi4 and diff(h) instead of int(psi4)
# to have coherent setting of integration constants and/or the same
# filtering for each term in the formula.
# from Nakano et al. PRD 91,104022

def spin_extrapolation(psi4,R,M,a,l=2,m=2,f0=0,lmax=5):
    psi4_in = psi4['l'+str(l)+'m'+str(m)]
    dt = psi4_in['time'][1]-psi4_in['time'][0]
    r = R*(1+M/(2*R))**2
    psi4_int2 = np.conj(NIntegratePsi4_filter(psi4_in,f0,2,0,M))
    psi4_int1 = der4(psi4_int2,dt)
    psi4NR = der4(psi4_int1,dt)
    if (l<lmax):
        psi4_inP = psi4['l'+str(l+1)+'m'+str(m)]
        psi4_int2P = np.conj(NIntegratePsi4_filter(psi4_inP,f0,2,0,M))
        psi4_int1P = der4(psi4_int2P,dt)
        psi4NRP = der4(psi4_int1P,dt)
        psi4NRdotP = der4(psi4NRP,dt)
    if (l>2):
        psi4_inM = psi4['l'+str(l-1)+'m'+str(m)]
        psi4_int2M = np.conj(NIntegratePsi4_filter(psi4_inM,f0,2,0,M))
        psi4_int1M = der4(psi4_int2M,dt)
        psi4NRM = der4(psi4_int1M,dt)
        psi4NRdotM = der4(psi4NRM,dt)
    psi_inf = (1-2*M/r)*(psi4NR - (l-1)*(l+2)/(2*r)*(psi4_int1) + ((l-1)*(l+2)*(l**2+l-4))/(8*r**2)*psi4_int2)
    if l<lmax:
        psi_inf=psi_inf + 1j*2*a/(l+1)**2*np.sqrt(((l+3)*(l-1)*(l+m+1)*(l-m+1))/((2*l+1)*(2*l+3)))*(psi4NRdotP-l*(l+3)/r*psi4NRdotP)
    if l > 2:
        psi_inf = psi_inf - 1j*2*a/l**2*np.sqrt(((l+2)*(l-2)*(l+m)*(l-m))/((2*l-1)*(2*l+1)))*(psi4NRdotM - (l-2)*(l+1)/r*psi4NRdotM)
    psi4_out=dict()
    psi4_out['re']=np.real(psi_inf)
    psi4_out['im']=np.imag(psi_inf)
    psi4_out['time']=psi4['l2m2']['time']
    return psi4_out


def second_order_extrapolation(psi4_in,R,M,l=2,m=2,order=2,int_method='time',f0=0):
    psi4NR = psi4_in['re']+1j*psi4_in['im']
    dt = psi4_in['time'][1]-psi4_in['time'][0]
    r = R*(1+M/(2*R))**2
    if int_method == 'time':
        #psi4_int1 = NIntegratePsi4(psi4_in,1,0,M,order-1)
        psi4_int2 = NIntegratePsi4(psi4_in,2,0,M,order)
    elif int_method == 'ffi':
        idx200 = np.searchsorted(RetardedTime(psi4_in['time'],r,M),200)
        idx400 = np.searchsorted(RetardedTime(psi4_in['time'],r,M),400)
        domega = np.diff(np.unwrap(-np.angle(psi4NR)))/dt
        f0_a = np.min(np.abs(domega[idx200:idx400]))
        f0 = np.abs(f0_a*m/(4*np.pi))
        #psi4_int1 = NIntegratePsi4_ffi(psi4NR,dt,f0,num_integrals=1)
        psi4_int2 = NIntegratePsi4_ffi(psi4NR,dt,f0,num_integrals=2)
    elif int_method == 'filter':
        #psi4_int1 = np.conj(NIntegratePsi4_filter(psi4_in,f0,1,0,M))
        psi4_int2 = np.conj(NIntegratePsi4_filter(psi4_in,f0,2,0,M))
    else:
        print ("Integration method not recongnized!")
        return;
#print r
    psi4_int1 = der4(psi4_int2,dt)
    psi4NR = der4(psi4_int1,dt)
    psi_inf = (1-2*M/r)*(psi4NR - (l-1)*(l+2)/(2*r)*(psi4_int1) + ((l-1)*(l+2)*(l**2+l-4))/(8*r**2)*psi4_int2)
    psi4_out=dict()
    psi4_out['re']=np.real(psi_inf)
    psi4_out['im']=np.imag(psi_inf)
    psi4_out['time']=psi4_in['time']
    return psi4_out

################################################################################
##
##  Master Extrapolation formula (17) return extrapolated \psi_4^{lm} 
##
##  Options: 
###    order_extr = 1,2 (default is 2)
##    int_method = 'time'   (as in BNS2015 with poly fit with 
##                            order_int_fit = #degree of polinomial)
##                 'ffi'    (formula (8) and (9) of MultiOrbits using f0)
##                 'filter  (using eq. 19 with h computed using filter)
##                          NIntegratePsi4_filter(psi4_in,f0=f0,2,0,M=M)
##                          (Integrate time with  #degree of polinomial =1 
##                                                and then filter with cut a f0)
##  Keep in mind that M = M_adm
##                    r = R*(1+M/(2*R))**2 R=cordinate radius of the dector
##
##  r = R*(1+M/(2*R))**2
##
##  Usualy we use as cut  f0=d['Omega0']/np.pi ---- The meassure units are always CU
##                        M = Madm of the system
##
##  psi_inf2 = Analysis.ExtrapolateAllPsi4(d['PSI4'],M=d['M0'],order_extr=2,int_method='filter',f0=f0)
##
##  psi_inf2 = Analysis.ExtrapolateAllPsi4(d['PSI4'],M=d['M0'],order_extr=2,ri=['700'],int_method='filter',f0=f0)
##  h_inf2   = Analysis.NIntegrateAllPsi4(psi_inf2['700'],2,M=d['M0'],int_method='filter',f0=f0)
##
################################################################################

def ExtrapolateAllPsi4(psi4,M,order_extr=2,ri=[],order_int_fit=2,int_method='filter',f0=0):
    psi4_inf = dict()
    if ri != []:
        r_list=list(set(ri).intersection(list(psi4)))
    else:
        r_list=list(psi4)
    for r in r_list:
        if int(r) != 0:
            psi4_inf[r]=dict()
            for mode in list(psi4[r]):
                REG = re.match("l(\d)m(-?)(\d)",mode)
                if REG:
                    l=int(REG.groups()[0])
                    m=int(REG.groups()[-1])
                else:
                    print ("Mode not recognized!")
                    return;
                if order_extr == 1:
                    psi4_inf[r][mode]=first_order_extrapolation(psi4[r][mode],int(r),M,l,m,order_int_fit,int_method,f0)
                elif order_extr == 2:
                    psi4_inf[r][mode]=second_order_extrapolation(psi4[r][mode],int(r),M,l,m,order_int_fit,int_method,f0)
                else:
                    print ("Only first or second order extrapolations!")
                    return;
    return psi4_inf


# Infinite extraction radius extrapolation with 1/r^N polynomial fit. 
def fit_extrapolation(tret,h,ri,M,modes='l2m2',order=2):
    if order == 2 :
        function = oneoverr_2
    elif order == 3:
        function = oneoverr_3
    elif order==4:
        function=oneoverr_4
    else:
        print ("Order not supported!")
        return;
    hfit=dict()
    hint,r_interp = interpolate_h(tret,h)
    print ('r_interp =',r_interp)
    R_interp=int(r_interp)*(1+M/(2*int(r_interp)))**2
    hfit[r_interp]=dict()
    if modes == 'all':
        for mode in hint[ri[0]]:
            A = np.empty((len(ri),len(hint[ri[0]][mode])))
            phi = np.empty((len(ri),len(hint[ri[0]][mode])))
            ri_int = np.array(ri).astype(int)    
            for i,r in enumerate(ri):
                R = int(r)*(1+M/(2*int(r)))**2
                A[i,:]=R*np.abs(hint[r][mode])
                phi[i,:]=np.unwrap(np.angle(hint[r][mode]))
            Afit = np.empty(len(hint[r_interp][mode]))
            phifit = np.empty(len(hint[r_interp][mode]))
            for j,t in enumerate(tret[r_interp]):
                par, pcov = scipy.optimize.curve_fit(function,ri_int,A[:,j])
                Afit[j]=par[0]
                par2,pcov2 = scipy.optimize.curve_fit(function,ri_int,phi[:,j])
                phifit[j]=par2[0]
            hfit[r_interp][mode]=Afit/R_interp*np.exp(1j*phifit)
    else:
        mode = modes
        A = np.empty((len(ri),len(hint[r_interp][mode])))
        phi = np.empty((len(ri),len(hint[r_interp][mode])))
        ri_int = np.array(ri).astype(int)    
        for i,r in enumerate(ri):
            A[i,:]=int(r)*np.abs(hint[r][mode])
            phi[i,:]=np.unwrap(np.angle(hint[r][mode]))
        Afit = np.empty(len(hint[r_interp][mode]))
        phifit = np.empty(len(hint[r_interp][mode]))
        for j,t in enumerate(tret[r_interp]):
            par, pcov = scipy.optimize.curve_fit(function,ri_int,A[:,j])
            Afit[j]=par[0]
            par2,pcov2 = scipy.optimize.curve_fit(function,ri_int,phi[:,j])
            phifit[j]=par2[0]
        hfit[r_interp][mode] = Afit/int(r_interp)*np.exp(1j*phifit)
    return hfit

# Functions for 1/r^N extrapolation

def oneoverr_2(r,a0,a1,a2):
    return a0+a1/r+a2/(r**2)
def oneoverr_3(r,a0,a1,a2,a3):
    return a0+a1/r+a2/(r**2)+a3/(r**3)
def oneoverr_4(r,a0,a1,a2,a3,a4):
    return a0+a1/r+a2/(r**2)+a3/(r**3)+a4/(r**4)

def interpolate_h(tret,h):
    R=str(np.max(np.array(tret.keys()).astype(int)))
    #print R
    int_h = dict()
    for r in list(h):
        int_h[r]=dict()
        for mode in list(h[r]):
            int_h[r][mode]=np.interp(tret[R],tret[r],np.real(h[r][mode])) + 1j* np.interp(tret[R],tret[r],np.imag(h[r][mode]))
    return int_h,R

################################################################
# Frequency integration methods.
################################################################

# FFI integration, from Reisswig&Pollnew CQG 28,195015

def NIntegratePsi4_ffi(psi4,dt,f0,num_integrals=1,window=False,padding=0,eps=0.1):
    N = len(psi4)
    if window:
        psi4=psi4*Planktaper(N,eps)
    psi4f = np.fft.fft(psi4,n=N*(1+padding))
    omega = np.fft.fftfreq(N*(1+padding),dt)
    for i in range(N*(1+padding)):
        if omega[i] == 0:
           omega[i] = 1e30
        div = 2*np.pi*omega[i]
        if abs(omega[i]) < abs(f0) :
            div = 2*np.pi*abs(f0)*np.sign(omega[i])
        psi4f[i] = (-1j)**(num_integrals) * psi4f[i]/(div**num_integrals)
    h = (np.fft.ifft(psi4f))[:N]
    return h

# FFI integration for all modes and extraction radii.

def NIntegrateAllPsi4_ffi(psi4,mass):
    h=dict()
    for r in list(psi4):
        if int(r)!=0:
            h[r]=dict()
        # First determine f0 from l=m=2:
            psi4c = psi4[r]['l2m2']['re']+1j*psi4[r]['l2m2']['im']
            dt = psi4[r]['l2m2']['time'][1]-psi4[r]['l2m2']['time'][0]
            domega = np.diff(np.unwrap(-np.angle(psi4c)))/dt
            idx200 = np.searchsorted(RetardedTime(psi4[r]['l2m2']['time'],int(r),mass),200)
            idx400 = np.searchsorted(RetardedTime(psi4[r]['l2m2']['time'],int(r),mass),400)
            f0_a = np.min(np.abs(domega[idx200:idx400]))
            f0 = np.abs(f0_a/(2*np.pi))
        #print 'f0= ', f0
            for mode in list(psi4[r]):
                REG = re.match("l(\d)m(-?)(\d)",mode)
                if REG:
                    m=int(REG.groups()[-1])
                else:
                    print ("Mode not recognized!")
                    return;
                f0m = f0*m/2.0
                print ('f0 ', mode, ' = ', f0m)
                psi4c = psi4[r][mode]['re']+1j*psi4[r][mode]['im']
                h[r][mode] = NIntegratePsi4_ffi(psi4c,dt,f0,num_integrals=2)
    return h



##########################################################################
# Standar integration setting only the "right" number of 
# integration constants and then applying a butterworth filter
# to the result. 
# f0 should be set as the initial frequency of Gravitational Waves.
##########################################################################

def NIntegratePsi4_filter(psi4,f0,Nint=2,r=0,M=0):
    order=Nint-1
    fnyq = 0.5/(np.diff(psi4['time'])[0])
    h = np.conj(NIntegratePsi4(psi4,Nint,r,M,order))
    b,a = scipy.signal.iirdesign(f0/fnyq,f0/fnyq/10.0,1e-2,80,ftype="butter")
    hfilt = scipy.signal.filtfilt(b,a,h,padtype='constant')
    return hfilt

##########################################################################
# Window Functions
#########################################################################

# Plank taper window, from McKechan et al. CQG 27,084020
# Needs testing and bugs fixing.

def Planktaper(time,dt,padding=1):
    t2=np.searchsorted(time,dt)
    N=len(time)
    eps=float(t2)/(2*N)
    tmp=np.ones(N*padding)
    for j in range(N*padding):
        j1=(padding-1)*N/2.0
        j2=(padding-1)*N/2.0*(1+2*eps)
        j3=(padding+1)*N/2.0*(1-2*eps)
        j4=(padding+1)*N/2.0
        if j <= j1:
            tmp[j] = 0
        elif j < j2 and j > j1:
            Z = (j2-j1)/(j-j1) + (j2-j1)/(j-j2)
            tmp[j]=1.0/(np.exp(Z)+1.0)
        elif j < j4 and j>j3:
            Z = (j3-j4)/(j-j3) + (j3-j4)/(j-j4)
            tmp[j]=1.0/(np.exp(Z)+1.0)
        elif j >= j4:
            tmp[j] = 0
    return tmp

def der4(array,step):
    out=np.array(array)
    #print out[0],out[1],out[2]
    out[0]=(-25.0*array[0]+48*array[1]-36*array[2]+16*array[3]-3*array[4])/12.0
    #print out[0]
    out[1]=(-3.0*array[0]-10*array[1]+18*array[2]-6*array[3]+array[4])/12.0
    for i in np.arange(2,(len(out)-2)):
        out[i]=(array[i-2]-8*array[i-1]+8*array[i+1]-array[i+2])/12.0
    out[-2]=(3.0*array[-1]+10*array[-2]-18*array[-3]+6*array[-4]-array[-5])/12.0
    out[-1]=(25.0*array[-1]-48*array[-2]+36*array[-3]-16*array[-4]+3*array[-5])/12.0
    return out/step


###########################################################
# Find the peaks of the FFT
#
# OPTION 1: scipy.signal.find_peaks_cwt
#   (http://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.find_peaks_cwt.html)
# OPTION 2: 
#
# More infos...
#
# https://github.com/MonsieurV/py-findpeaks
# http://www.ncbi.nlm.nih.gov/pmc/articles/PMC2631518/
# https://en.wikipedia.org/wiki/Sinusoidal_model
# https://en.wikipedia.org/wiki/Savitzky–Golay_filter
##########################################################


##########################################################################
#
#  General purpose filter (Low-High Pass)
#
##########################################################################
def LowPassFilter(t,x,freq) :
   dt = np.diff(t[:2])
   fn = 0.5/dt
   b, a = scipy.signal.butter(4, freq /fn )
   return  scipy.signal.filtfilt(b, a, x)
def HighPassFilter(t,x,freq) :
   dt = np.diff(t[:2])
   fn = 0.5/dt
   b, a = scipy.signal.butter(4, freq /fn ,btype='highpass')
   return  scipy.signal.filtfilt(b, a, x)


##########################################################################
#
#  ChirpMass Related
#
##########################################################################

def time_to_merge(f0):
    return (5.0/(256.0 * chirpmass**(5.0/3.0)))*1.0/((np.pi*f0)**(8.0/3.0))
def chirp_frequency(t,t0,f0,chirpmass) :
    return (f0 /
            (1 - (256/5)* ( (np.pi*f0)**(8.0/3.0) ) * (chirpmass**(5.0/3.0)) * (t-t0)
            )**(3.0/8.0))

def chirp_mass(m1,m2) :
    return (m1*m2)**(3.0/5.0)/((m1+m2)**0.2)



