#!/usr/bin/env python

import sys
import getopt
 
import os
import re
import glob
import time
import numpy as np
import bz2
import gzip


class search :
    pass

search.OUTPUT   ='(\S*)/output-(\d\d\d\d)/(\S*)'

## -------------------------------------------------------
##  SearchSimfactoryOutputs(File) ==> File or list of restart [dir0,....,dirN]
##          If File is a "FILE" return File            
##          Return a list of files or  DIRs  
##          whose name match: search.OUTPUT ='(\S*)/output-(\d\d\d\d)/(\S*)'
## -------------------------------------------------------

def SearchSimfactoryOutputs(File) :
     if os.path.isfile(File) :
         pass; 
         print "Is file"
     elif  os.path.isdir(File) :
         pass;
         print "Is dir"
     else :
         return  []
     x = re.search(search.OUTPUT,File)
     if x :
         restarts = glob.glob(x.groups()[0]+'/output-????/'+x.groups()[2])
     else :
         restarts = [File]
     return restarts

def BaseSimfactoyDir(File) :
     if os.path.isfile(File) :
         pass; 
         print "Is file"
     elif  os.path.isdir(File) :
         pass;
         print "Is dir"
     else :
         return  []
     x = re.search(search.OUTPUT,File)
     if x :
         base_dir = str(x.groups()[0])
         del x
     elif (os.path.exists(os.path.join(File, "SIMFACTORY"))) :
         base_dir = File
         pass;
     else :
         return ''
     return base_dir

def RestartSimfactoyDirs(File) :
    base_dir = BaseSimfactoyDir(File) 
    return np.sort(glob.glob(base_dir+'/output-????/'))
